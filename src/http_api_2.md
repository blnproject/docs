<style>
table {
    width:100%;
}
</style>

# HTTP API version 2

API web-server listens on [http://localhost:8283/](http://localhost:8283/) It can be configured in config/rpc.conf   

Path of all methods is prefixed with /api/v2   

Encoding of values:   
- ```amount:```                [satoshis](https://en.bitcoin.it/wiki/Satoshi_(unit)) (1 coin = 100000000 satoshi)         
- ```data:```                  JSON          
- ```hash:```                  HEX-string    
- ```boolean:```               string "true" or "false"
- ```string:```                UTF-8 string

Errors are returned with HTTP status 400 Bad Request.  

Example of GET request: [http://localhost:8283/api/v2/node](http://localhost:8283/api/v2/node)

### Node API

#### Node information	
- Request
    - Method: **GET**
    - URL: ```/api/v2/node```
- Response
    - ```Body```: Returns data about this node.
    ```
    {
        "agent": "/Blacknet:0.2.9/",
        "name": "Blacknet",
        "version": "0.2.9",
        "protocolVersion": 12,
        "outgoing": 8,
        "incoming": 39,
        "listening": [
            "139.162.105.187:28453",
            "0.0.0.0:28453",
            "[2400:8902:0:0:f03c:92ff:fee0:c6d]:28453"
        ],
        "warnings": []
    }
    ```
#### Peers information		
- Request
    - Method: **GET**
    - URL: ```/api/v2/peers```
- Response
    - ```Body```: Returns data about each connected network node.
    ```
    [
        {
            "peerId": 2,
            "remoteAddress": "5.189.152.114:28453",
            "localAddress": "139.162.105.187:47744",
            "timeOffset": 0,
            "ping": 229,
            "protocolVersion": 12,
            "agent": "/Blacknet:0.2.5/",
            "outgoing": true,
            "banScore": 0,
            "feeFilter": "100000",
            "connectedAt": 1593358148,
            "lastChain": {
                "chain": "80F3785D2BA1F813B5DC38396119BA13D24A0B9D55AFA5C1E4D75A77777A0591",
                "cumulativeDifficulty": "300336976098487120057988",
                "fork": false
            },
            "totalBytesRead": 35071309,
            "totalBytesWritten": 8338386
        }
        ...
    ]
    ```

#### Ledger information
- Request
    - Method: **GET**
    - URL: ```/api/v2/ledger```
- Response
    - ```Body```: Returns data about current state of ledger.
    ```
    {
        "height": 1543802,
        "blockHash": "3099631A53AA9EC74152D9618DDA18020952ECAB715A28FF51D5F3278735ADCA",
        "blockTime": 1596010716,
        "rollingCheckpoint": "917BFAF4EBEED82E24DB615BABA6B283FFC53FF3B4034B16308D19BE312B174B",
        "difficulty": "652458787252640804660971440917251737174934657204542678423860",
        "cumulativeDifficulty": "300340992009831398532037",
        "supply": "101533260707395509",
        "maxBlockSize": 100000,
        "nxtrng": "76D6BE137EC07D6E0BC1A689CD0F879525E1D43FB57CB7D00287E21E49E6B2CF"
    }
    ```

#### Transaction pool info	
- Request
    - Method: **GET**
    - URL: ```/api/v2/txpool```
- Response
    - ```Body```: Returns data about memory pool of transactions.

#### Get block	
- Request
    - Method: **GET**
    - URL: ```/api/v2/block/{hash}/{txdetail?}```
    - Parameters: 
        - ```hash:``` hash of block
        - ```txdetail:``` true to include transaction data (optional boolean, default false)
- Response
    - ```Body```: Returns data of a block with given block-hash.

#### Get block index	
- Request
    - Method: **GET**
    - URL: ```/api/v2/blockindex/{hash}```
    - Parameters: 
        - ```hash:``` hash of block
- Response
    - ```Body```: Returns index of a block with given block-hash.

#### Get block hash	
- Request
    - Method: **GET**
    - URL: ```/api/v2/blockhash/{height}```
    - Parameters: 
        - ```height:``` height of block
- Response
    - ```Body```: This method may be slow. Returns hash of a block with given block-height.

#### Account information
- Request
    - Method: **GET**
    - URL: ```/api/v2/account/{address}/{confirmations?}```
    - Parameters: 
        - ```address:``` address of account
        - ```confirmations:``` number of confirmations for confirmed balance (optional, default 10)
- Response
    - ```Body```: Returns data of an account with given address.

#### Generate new account	
- Request
    - Method: **GET**
    - URL: ```/api/v2/generateaccount```
- Response
    - ```Body```: Returns data of new account. Save mnemonic before using account.

#### Address information		
- Request
    - Method: **GET**
    - URL: ```/api/v2/address/{address}```
    - Parameters: 
        - ```address:``` address of account
- Response
    - ```Body```: Returns data about address: public key.

#### Mnemonic information		
- Request
    - Method: **POST**
    - URL: ```/api/v2/mnemonic```
    - Formdata: 
        - ```mnemonic:``` mnemonic of account
- Response
    - ```Body```: Returns data about mnemonic: address and public key.

#### Send raw transaction	
- Request
    - Method: **GET**
    - URL: ```/api/v2/sendrawtransaction/{hex}```
    - Parameters: 
        - ```hex:``` HEX-encoded transaction
- Response
    - ```Body```: Returns transaction hash.

#### Decrypt message	
- Request
    - Method: **POST**
    - URL: ```/api/v2/decryptmessage```
    - Formdata: 
        - ```mnemonic:``` mnemonic of receiver
        - ```from:``` address of sender
        - ```message:``` encrypted message
- Response
    - ```Body```: Returns string.

#### Sign message	
- Request
    - Method: **POST**
    - URL: ```/api/v2/signmessage```
    - Formdata: 
        - ```mnemonic:``` mnemonic of signing account
        - ```message:``` text message
- Response
    - ```Body```: Returns signature string.

#### Verify message	
- Request
    - Method: **GET**
    - URL: ```/api/v2/verifymessage/{from}/{signature}/{message}```
    - Parameters: 
        - ```from:``` address of signing account
        - ```signature:``` signature to verify
        - ```message:``` text message
- Response
    - ```Body```: Returns boolean.

#### Add peer	
- Request
    - Method: **GET**
    - URL: ```/api/v2/addpeer/{address}/{port?}```
    - Parameters: 
        - ```address:``` network address
        - ```port:``` network port (optional, default 28453)
        - ```force:``` force connection (optional boolean, default false)
- Response
    - ```Body```: Returns boolean.

#### Disconnect peer	
- Request
    - Method: **GET**
    - URL: ```/api/v2/disconnectpeer/{id}```
    - Parameters: 
        - ```id:``` peer id
- Response
    - ```Body```: Returns boolean.

#### Start staking	
- Request
    - Method: **POST**
    - URL: ```/api/v2/startstaking```
    - Formdata: 
        - ```mnemonic:``` mnemonic of account
- Response
    - ```Body```: Returns boolean.

#### Stop staking	
- Request
    - Method: **POST**
    - URL: ```/api/v2/stopstaking```
    - Formdata: 
        - ```mnemonic:``` mnemonic of account
- Response
    - ```Body```: Returns boolean.

#### Status of staking	
- Request
    - Method: **POST**
    - URL: ```/api/v2/isstaking```
    - Formdata: 
        - ```mnemonic:``` mnemonic of account
- Response
    - ```Body```: Returns boolean.

### Wallet API

#### Transfer
- Request
    - Method: **POST**
    - URL: ```/api/v2/transfer```
    - Formdata: 
        - ```mnemonic:``` mnemonic of wallet
        - ```fee:``` transaction fee
        - ```account:``` amount to transfer
        - ```to:``` address of receiving account
        - ```message:``` text message (optional)
        - ```encrypted:``` 0 for plain message, 1 for encrypted (optional, default 0)
- Response
    - ```Body```: Returns transaction hash.

#### Burn
- Request
    - Method: **POST**
    - URL: ```/api/v2/burn```
    - Formdata: 
        - ```mnemonic:``` mnemonic of wallet
        - ```fee:``` transaction fee
        - ```account:``` amount to burn
        - ```message:``` text message (optional)
- Response
    - ```Body```: Returns transaction hash.

#### Lease
- Request
    - Method: **POST**
    - URL: ```/api/v2/lease```
    - Formdata: 
        - ```mnemonic:``` mnemonic of wallet
        - ```fee:``` transaction fee
        - ```account:``` amount to lease
        - ```to:``` address of receiving account
- Response
    - ```Body```: Returns transaction hash.

#### Cancel Lease
- Request
    - Method: **POST**
    - URL: ```/api/v2/cancellease```
    - Formdata: 
        - ```mnemonic:``` mnemonic of wallet
        - ```fee:``` transaction fee
        - ```account:``` leased amount
        - ```to:``` address of receiving account
        - ```height:``` height of lease
- Response
    - ```Body```: Returns transaction hash.

#### Get wallet transactions	
- Request
    - Method: **GET**
    - URL: ```/api/v2/wallet/{address}/transactions```
    - Parameters: 
        - ```address:``` address of wallet
- Response
    - ```Body```: Returns data of all transactions in wallet.

#### Get transaction	
- Request
    - Method: **GET**
    - URL: ```/api/v2/wallet/{address}/transaction/{hash}/{raw?}```
    - Parameters: 
        - ```address:``` address of wallet
        - ```hash:``` hash of transaction
        - ```raw:``` true to return HEX (optional boolean, default false)
- Response
    - ```Body```: Returns data of transaction.

#### Get confirmations	
- Request
    - Method: **GET**
    - URL: ```/api/v2/wallet/{address}/confirmations/{hash}```
    - Parameters: 
        - ```address:``` address of wallet
        - ```hash:``` hash of transaction
- Response
    - ```Body```: Returns number of confirmations for transaction.

#### Get outgoing leases	
- Request
    - Method: **GET**
    - URL: ```/api/v2/wallet/{address}/outleases```
    - Parameters: 
        - ```address:``` address of wallet
- Response
    - ```Body```: Returns list of outgoing leases.

#### Get sequence number	
- Request
    - Method: **GET**
    - URL: ```/api/v2/wallet/{address}/sequence```
    - Parameters: 
        - ```address:``` address of wallet
- Response
    - ```Body```: Returns sequence number of wallet.

#### Get reference chain	
- Request
    - Method: **GET**
    - URL: ```/api/v2/wallet/{address}/referencechain```
    - Parameters: 
        - ```address:``` address of wallet
- Response
    - ```Body```: Returns reference chain of wallet.

### Notifications

The web-server accepts WebSocket connections on path /websocket

Requests and responses are JSON objects.
- New block
```
{ command: "subscribe", route: "block" }
```
- New transaction in tx pool
```
{ command: "subscribe", route: "txpool" }
```
- New transaction in wallet
```
{ command: "subscribe", route: "wallet", address: "$address" }
```

